/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac.android.visitor;

import cl.uchile.dcc.bnac.*;

import java.util.*;
import java.io.*;
import java.net.*;
import android.app.*;
import android.os.*;
import android.widget.*;
import android.view.*;
import android.os.*;
import android.view.inputmethod.*;
import android.content.*;
import android.content.res.*;
import android.text.format.*;
import android.net.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BnacVisitor extends Activity
    implements AdapterView.OnItemClickListener
{
    static Logger l = LoggerFactory.getLogger(BnacVisitor.class);

    protected String addr;
    protected int port;
    protected String proj;

    protected MuseumEnvironment me;
    protected InfoSet info;

    protected boolean home;

    protected RouteItem[] routeItems;
    protected int routeIdx;
    protected int routeCapIdx;

    protected int likes = 0;
    protected List<Comment> comments = new ArrayList<Comment>();

    protected Stack<InfoSet.InfoPage> pageHistory;

    static final int DIALOG_SELECTSERVER_ID = 0;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        BnacLog.info("BnacVisitor started");
        BnacLog.debug("Locale: %s", Locale.getDefault().getLanguage());

        me = null;
        info = null;

        home = true;

        SharedPreferences pref = getPreferences(MODE_PRIVATE);
        addr = pref.getString("server.address", "");
        port = pref.getInt("server.port", 0);
        proj = pref.getString("project.path", "");

        if (!addr.equals("") && port > 0) {
            BnacLog.info("using server %s:%d", addr, port);
        } else {
            addr = "";
            port = 0;
            BnacLog.info("server info not set");
        }

        if (proj.length() > 0) {
            BnacLog.info("project path %s", proj);
            try {
                me = MuseumEnvironment.fromFile(
                        new File(proj, "project.xml"));
                info = InfoSet.fromFile(
                        new File(proj, "info.xml"));
            } catch (Exception e) {
                BnacLog.info("unable to load project: %s", e.getMessage());
            }
        } else {
            BnacLog.info("project path not set");
        }
    }

    public boolean onCreateOptionsMenu (Menu menu)
    {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu (Menu menu)
    {
        menu.clear();
        if (home) {
            menu.add(0, R.id.select_project, 0, R.string.select_project);
            menu.add(0, R.id.select_server, 1, R.string.select_server);
        } else {
            menu.add(0, R.id.back_to_start_screen, 0,
                    R.string.back_to_start_screen);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected (MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.select_server:
                BnacLog.debug("select server");
                selectServer();
                return true;
            case R.id.select_project:
                BnacLog.debug("select project");
                selectProject();
                return true;
            case R.id.back_to_start_screen:
                BnacLog.debug("back to showRouteList screen");
                backToHomeScreen();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void backToHomeScreen ()
    {
        home = true;
        setTitle(R.string.app_name);
        setContentView(R.layout.main);
    }

    protected void selectProject ()
    {
        Intent intent = new Intent("org.openintents.action.PICK_DIRECTORY");
        intent.setData(Uri.parse("file:///mnt/sdcard/"));
        intent.putExtra("org.openintents.extra.TITLE",
                getString(R.string.app_name));

        try {
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            Toast.makeText(this, anfe.getMessage(), Toast.LENGTH_LONG).show();
            Toast.makeText(this, "u mad programmerfag?", Toast.LENGTH_SHORT).
                show();
        }

    }

    protected void selectServer ()
    {
        setContentView(R.layout.server_selection);

        EditText tfAddr = (EditText) findViewById(R.id.server_address);
        EditText tfPort = (EditText) findViewById(R.id.server_port);

        if (!addr.equals("") && port > 0) {
            BnacLog.debug("server %s:%d", addr, port);
            tfAddr.setText(addr);
            tfPort.setText(String.format("%d", port));
        } else {
            BnacLog.debug("invalid server %s:%d", addr, port);
        }
    }

    public void setServer (View view)
    {
        InputMethodManager imm =
            (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        EditText tfAddr = (EditText) findViewById(R.id.server_address);
        EditText tfPort = (EditText) findViewById(R.id.server_port);

        BnacLog.debug("tfAddr: %s", tfAddr.getText());
        BnacLog.debug("tfPort: %s", tfPort.getText());

        InetAddress addr2 = null;
        int port2 = 0;

        try {
            addr2 = InetAddress.getByName(tfAddr.getText().toString());
        } catch (Exception e) {
            BnacLog.debug("invalid address: %s", e.getMessage());
        }

        try {
            port2 = Integer.parseInt(tfPort.getText().toString());
        } catch (Exception e) {
            BnacLog.debug("invalid port: %s", e.getMessage());
        }

        if (addr2 != null && port2 > 0) {
            SharedPreferences.Editor ed = getPreferences(MODE_PRIVATE).edit();
            ed.putString("server.address", tfAddr.getText().toString());
            ed.putInt("server.port", port2);
            ed.commit();
            addr = tfAddr.getText().toString();
            port = port2;
            BnacLog.info("setting server %s:%d",
                    addr2.getHostAddress(), port2);
        } else {
            Toast.makeText(this, getString(R.string.invalid_input),
                    Toast.LENGTH_SHORT).show();
            BnacLog.info("invalid server %s:%s",
                    tfAddr.getText(), tfPort.getText());
        }

        setContentView(R.layout.main);
    }

    protected void onActivityResult (int req, int res, Intent data)
    {
        super.onActivityResult(req, res, data);

        BnacLog.debug(String.format("onActivityResult: %d %d", req, res));

        switch (req) {
            case 0:
                if (res == RESULT_OK
                        && data != null
                        && data.getDataString() != null) {
                    try {
                        proj = data.getDataString().substring(7);
                        File file = new File(proj, "project.xml");
                        BnacLog.debug(file.getAbsolutePath());
                        me = MuseumEnvironment.fromFile(file);
                        info = InfoSet.fromFile(
                                new File(proj, "info.xml"));

                        BnacLog.debug("MuseumEnvironment: %s", me.getTitle());

                        SharedPreferences.Editor ed =
                            getPreferences(MODE_PRIVATE).edit();
                        ed.putString("project.path", proj);
                        ed.commit();
                    } catch (Exception e) {
                        BnacLog.debug(e.toString());
                        Toast.makeText(this, "u mad programmerfag?",
                                    Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                Toast.makeText(this, "what just happened?",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void showRouteList (View view)
    {
        if (me == null) {
            BnacLog.debug("no project is set");
            Toast.makeText(this, R.string.please_set_project,
                    Toast.LENGTH_LONG).show();
            return;
        }

        BnacLog.debug("loading routes");

        home = false;

        Route[] routes = me.routeArray();
        routeItems = new RouteItem[routes.length];
        int i=0;
        for (Route r: routes) { routeItems[i++] = new RouteItem(r); }

        BnacLog.debug("loaded %d routes", routes.length);

        ListView lv = new ListView(this);
        lv.setAdapter(new ArrayAdapter<RouteItem>(this, R.layout.route_item,
                    routeItems));
        lv.setTextFilterEnabled(true);

        lv.setOnItemClickListener(this);

        setTitle(R.string.select_route);
        setContentView(lv);

        routeCapIdx = 0;
    }

    public void onItemClick (AdapterView<?> parent, View view,
        int position, long id) {
        BnacLog.debug("route %s", routeItems[position].getRoute().getId());

        routeIdx = position;
        setTitle(routeItems[position].toString());
        setContentView(R.layout.capview);
        loadCapture();
    }

    public void loadCapture ()
    {
        View bn_like = findViewById(R.id.bn_like);
        bn_like.setVisibility(View.VISIBLE);

        TextView tv;
        Capture cap = me.getCapture(routeItems[routeIdx].getRoute().
            captureArray()[routeCapIdx]);
        CObject obj = cap.getReferencedObject();

        try {
            ImageView iv = (ImageView) findViewById(R.id.img);
            iv.setImageURI(Uri.parse(String.format("file://%s/%s",
                            proj, cap.get("Resource"))));
        } catch (Exception e) {
            Toast.makeText(this, "u mad programmerfag?", Toast.LENGTH_SHORT).
                show();
        }

        /* capture data */
      
        tv = (TextView) findViewById(R.id.author);
        tv.setText(getString(obj.getId(), "Author", obj.get("Author")));
        tv = (TextView) findViewById(R.id.title);
        tv.setText(getString(obj.getId(), "Title", obj.get("Title")));
        tv = (TextView) findViewById(R.id.type);
        tv.setText(getString(obj.getId(), "Type", obj.get("Type")));
        tv = (TextView) findViewById(R.id.dims);
        tv.setText(String.format("%s x %s", obj.get("Width"),
                    obj.get("Height")));

        /* likes */
        likes = getLikes();
        setUpLikes();

        /* comments */
        comments = getComments();
        setUpComments();
    }

    protected List<Map<String, String>> commentGroup ()
    {
        List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
        Hashtable<String, String> tab = new Hashtable<String, String>();
        tab.put("commentHeader",
                String.format(getString(R.string.comments),
                    comments.size()));
        ret.add(tab);
        return ret;
    }

    protected List<List<Map<String, String>>> commentChildList ()
    {
        List<List<Map<String, String>>> ret =
            new ArrayList<List<Map<String, String>>>();
        List<Map<String, String>> comms =
            new ArrayList<Map<String, String>>();
        ret.add(comms);

        Map<String, String> comm;
        for (Comment c: comments) {
            comm = new Hashtable<String, String>();
            comm.put("commName", c.name);
            comm.put("commDate", c.getPrettyDate());
            comm.put("commCont", c.cont);
            comms.add(comm);
        }

        return ret;
    }

    protected Socket makeSocket () throws Exception
    {
        if (addr.length() > 0 && port > 0) {
            Socket sock = new Socket();
            sock.setSoTimeout(5000);
            sock.connect(new InetSocketAddress(addr, port));
            return sock;
        } else {
            Toast.makeText(this, getString(R.string.please_set_server),
                        Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    protected int getLikes ()
    {
        try {
            Capture cap = me.getCapture(routeItems[routeIdx].getRoute().
                captureArray()[routeCapIdx]);
            int ret;
            Socket sock = makeSocket();

            if (sock == null) { return -1; }

            BufferedReader in = new BufferedReader(new InputStreamReader(
                        sock.getInputStream()));
            PrintWriter out = new PrintWriter(sock.getOutputStream());

            out.println("Likes");
            out.flush();
            in.readLine(); /* OK */
            out.println(cap.getId());
            out.flush();
            String str = in.readLine(); /* <num> */
            BnacLog.debug("Likes: got %s from server", str);
            in.readLine(); /* BYE */
            sock.close();

            try {
                ret = Integer.parseInt(str);
                return ret;
            } catch (Exception e) {
                BnacLog.error("could not parseInt(%s)", str);
            }

        } catch (Exception e) {
            BnacLog.error("server error: %s", e.getMessage());
        }

        return -1;
    }

    public void doLike (View view)
    {
        try {
            Socket sock = makeSocket();

            if (sock == null) { return; }

            Capture cap = me.getCapture(routeItems[routeIdx].getRoute().
                captureArray()[routeCapIdx]);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                        sock.getInputStream()));
            PrintWriter out = new PrintWriter(sock.getOutputStream());

            out.println("PutLike");
            out.flush();
            in.readLine(); /* OK */
            out.println(cap.getId());
            out.flush();
            in.readLine(); /* BYE */
            sock.close();

            view.setVisibility(View.GONE);
            ++likes;
            setUpLikes();
        } catch (Exception e) {
            BnacLog.error("server error: %s", e.getMessage());
            Toast.makeText(this, getString(R.string.server_error),
                        Toast.LENGTH_SHORT).show();
        }
    }

    protected void setUpLikes ()
    {
        TextView tv = (TextView) findViewById(R.id.likes);
        if (likes > -1) {
            tv.setText(String.format(getString(R.string.likes), likes));
        } else {
            tv.setText(getString(R.string.server_error));
        }
    }

    protected List<Comment> getComments ()
    {
        try {
            Socket sock = makeSocket();

            if (sock == null) { return null; }

            List<Comment> ret = new ArrayList<Comment>();
            Capture cap = me.getCapture(routeItems[routeIdx].getRoute().
                captureArray()[routeCapIdx]);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                        sock.getInputStream()));
            PrintWriter out = new PrintWriter(sock.getOutputStream());

            out.println("Comments");
            out.flush();
            in.readLine(); /* OK */
            out.println(cap.getId());
            out.flush();
            in.readLine(); /* OK */

            String str;
            String tstamp, name, cont;
            Comment c;
            while (!(str = in.readLine()).equals("BYE")) {
                tstamp = str;
                name = in.readLine();
                cont = "";
                while (!(str = in.readLine()).equals(".")) {
                    cont = String.format("%s%n%s", cont, str);
                }
                cont = cont.trim();

                BnacLog.debug("comment { %s, %s, %s }",
                        tstamp, name, cont);
                try {
                    c = new Comment(name, Long.parseLong(tstamp), cont);
                    BnacLog.debug("comment:%nname: %s%ndate: %s (%d)%n%s",
                            c.name, c.getPrettyDate(), c.date, c.cont);
                    ret.add(c);
                } catch (Exception e) {
                    BnacLog.error("could not parseLong(%s)", tstamp);
                }
            }
            sock.close();

            return ret;
        } catch (Exception e) {
            BnacLog.error("server error: %s", e.getMessage());
            return null;
        }
    }

    public void sendComment (View view)
    {
        TextView tv_name = (TextView) findViewById(R.id.new_comm_name);
        TextView tv_cont = (TextView) findViewById(R.id.new_comm_cont);

        Comment c = new Comment(
                tv_name.getText().toString(),
                System.currentTimeMillis(),
                tv_cont.getText().toString());

        BnacLog.debug("sending:%nname:\t%s%ndate:\t%s (%d)%n%s",
                c.name, c.getPrettyDate(), c.date, c.cont);

        try {
            Socket sock = makeSocket();

            if (sock == null) { return; }

            Capture cap = me.getCapture(routeItems[routeIdx].getRoute().
                captureArray()[routeCapIdx]);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                        sock.getInputStream()));
            PrintWriter out = new PrintWriter(sock.getOutputStream());

            out.println("PutComment");
            out.flush();
            in.readLine(); /* OK */
            out.println(cap.getId());
            out.println(c.name);
            out.println(c.cont);
            out.println(".");
            out.flush();
            in.readLine(); /* BYE */
            sock.close();

            BnacLog.debug("comment sent :D");

            comments.add(c);

            Toast.makeText(this, getString(R.string.sent_successfully),
                    Toast.LENGTH_SHORT).show();

            tv_name.setText("");
            tv_cont.setText("");

            setUpComments();
        } catch (Exception e) {
            BnacLog.error("server error: %s", e.getMessage());
            Toast.makeText(this, getString(R.string.server_error),
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void setUpComments ()
    {
        ExpandableListView elv =
            (ExpandableListView) findViewById(R.id.comm_list);
        elv.setVisibility(View.VISIBLE);
        if (comments != null) {
            SimpleExpandableListAdapter sela =
                new SimpleExpandableListAdapter(
                        this,
                        commentGroup(),
                        R.layout.comm_group,
                        new String[] { "commentHeader" },
                        new int[] { R.id.comm_header },
                        commentChildList(),
                        R.layout.comm_item,
                        new String[] {
                            "commName",
                            "commDate",
                            "commCont"
                        },
                        new int[] {
                            R.id.comm_name,
                            R.id.comm_date,
                            R.id.comm_cont
                        });
            elv.setAdapter(sela);
        } else {
            elv.setVisibility(View.GONE);
            Toast.makeText(this, getString(R.string.could_not_comments),
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected String getString (String id, String key, String df)
    {
        BnacLog.debug("getting [%s][%s][%s]",
                id, Locale.getDefault().getLanguage(), key);
        String ret = me.getString(id, Locale.getDefault().getLanguage(), key);
        if (ret != null) {
            BnacLog.debug("found it :D");
            return ret;
        }
        BnacLog.debug("couldn't find it :(");
        return df;
    }

    public void prev (View view)
    {
        int capNo = routeItems[routeIdx].getRoute().captureNo();
        routeCapIdx = (routeCapIdx>0)?routeCapIdx-1:(capNo-1);
        loadCapture();
    }

    public void next (View view)
    {
        int capNo = routeItems[routeIdx].getRoute().captureNo();
        routeCapIdx = (routeCapIdx+1)%capNo;
        loadCapture();
    }

    public void showInformation (View view)
    {
        Capture cap = me.getCapture(routeItems[routeIdx].getRoute().
            captureArray()[routeCapIdx]);
        CObject obj = cap.getReferencedObject();

        InfoSet.InfoPage page = info.getPage(cap.getId());

        if (page == null) {
            BnacLog.debug("could not find page for %s", cap.getId());
            Toast.makeText(this, R.string.info_not_available,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        pageHistory = new Stack<InfoSet.InfoPage>();
        pageHistory.push(page);
        BnacLog.debug("pushing into stack page %s", page.getId());

        setContentView(R.layout.info);
        loadInfoPage();
    }

    protected void loadInfoPage ()
    {
        InfoSet.InfoPage page = pageHistory.peek();
        InfoSet.InfoEntry entry =
            page.getEntry(Locale.getDefault().getLanguage());

        TextView descr = (TextView) findViewById(R.id.descr);
        ImageView img = (ImageView) findViewById(R.id.img);

        if (page.getImage() != null) {
            try {
                img.setVisibility(View.VISIBLE);
                img.setImageURI(Uri.parse(String.format("file://%s/%s",
                                proj, page.getImage())));
            } catch (Exception e) {
                BnacLog.error("could not display image %s/%s",
                        proj, page.getImage());
            }
        } else {
            img.setVisibility(View.GONE);
        }

        if (entry == null) {
            BnacLog.warn("null entry: %s[%s]",
                    page.getId(), Locale.getDefault().getLanguage());
            setTitle(R.string.no_translation);
            descr.setText(R.string.no_translation);
            return;
        }

        setTitle(entry.title);
        descr.setText(entry.descr);
    }

    public void prevInfo (View view)
    {
        if (pageHistory.size() > 1) {
            pageHistory.pop();
            loadInfoPage();
        } else {
            Toast.makeText(this, getString(R.string.this_is_beginning),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void nextInfo (View view)
    {
        InfoSet.InfoPage page = pageHistory.peek();
        if (page.getNext() != null) {
            pageHistory.push(info.getPage(page.getNext()));
            loadInfoPage();
        } else {
            Toast.makeText(this, getString(R.string.this_is_end),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void exitFromInfo (View view)
    {
        setTitle(routeItems[routeIdx].toString());
        setContentView(R.layout.capview);
        loadCapture();
    }

    class RouteItem
    {
        protected Route route;

        public RouteItem (Route route)
        {
            this.route = route;
        }

        public String toString ()
        {
            String ret = me.getString(route.getId(),
                    Locale.getDefault().getLanguage(), "Name");
            if (ret != null) { return ret; }
            return route.getId();
        }

        public Route getRoute () { return route; }
    }

    class Comment
    {
        public String name;
        public long date;
        public String cont;

        public Comment (String name, long date, String cont)
        {
            this.name = name;
            this.date = date;
            this.cont = cont;
        }

        public String getPrettyDate ()
        {
            return DateFormat.format(
                    getString(R.string.date_format), date).toString();
        }
    }
}

